use ctrlc;
use ksway::IpcEvent;
use serde::{Deserialize, Serialize};
use std::str;
use std::sync::mpsc::channel;

mod cli;

#[derive(Serialize, Deserialize)]
struct WindowEvent {
    change: String,
}

fn set_opacity(client: &mut ksway::Client, inactive_opacity: f32) {
    match client.run(format!("[tiling] opacity {}; opacity 1", inactive_opacity)) {
        Err(e) => panic!("Error running command: {}", e),
        _ => {}
    }
}

fn handle_window_event(client: &mut ksway::Client, inactive_opacity: f32, payload: Vec<u8>) {
    let s = match str::from_utf8(&payload) {
        Ok(v) => v,
        Err(e) => {
            println!("Invalid UTF-8 sequence: {}", e);
            "{}"
        }
    };
    let we: WindowEvent = match serde_json::from_str(s) {
        Ok(v) => v,
        _ => {
            println!("Error parsing window event: {}", s);
            return;
        }
    };
    if we.change == "focus" {
        set_opacity(client, inactive_opacity);
    }
}

fn main() {
    let args = cli::parse_args();
    let (quit_tx, quit_rx) = channel();
    let mut client = match ksway::Client::connect() {
        Ok(v) => v,
        Err(e) => panic!("Error connecting to sway socket: {}", e),
    };

    set_opacity(&mut client, args.inactive_opacity);

    let rx = match client.subscribe(vec![IpcEvent::Window, IpcEvent::Tick]) {
        Ok(v) => v,
        Err(e) => panic!("Error subscribing to sway events: {}", e),
    };

    ctrlc::set_handler(move || quit_tx.send(1).unwrap()).expect("Error setting Ctrl-C handler");

    loop {
        if quit_rx.try_recv().is_ok() {
            println!("Resetting opactity to 1");
            match client.run("[tiling] opacity 1") {
                Err(e) => panic!("Error running command: {}", e),
                _ => break,
            }
        }
        while let Ok((payload_type, payload)) = rx.try_recv() {
            match payload_type {
                IpcEvent::Window => {
                    handle_window_event(&mut client, args.inactive_opacity, payload)
                }
                _ => {}
            }
        }
        match client.poll() {
            Err(e) => panic!("Error polling: {}", e),
            _ => {}
        };
    }
}
