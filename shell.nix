{ pkgs ? import <nixpkgs> { } }:
let unstable = import <nixos-unstable> { };
in pkgs.mkShell {
  nativeBuildInputs = [ unstable.rustc unstable.cargo unstable.rust-analyzer unstable.rustfmt ];
}
