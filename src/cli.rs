use clap::Parser;

fn less_than_1(s: &str) -> Result<f32, String> {
    match s.parse::<f32>() {
        Err(e) => Result::Err(e.to_string()),
        Ok(v) => {
            if 0.0 < v && v < 1.0 {
                Result::Ok(v)
            } else {
                return Result::Err("Opacity needs to be between 0 and 1".to_string());
            }
        }
    }
}

/// Set different opacity for inactive windows in sway
#[derive(Parser)]
pub struct Cli {
    /// The opactity of inactive windows
    #[arg(default_value_t=0.7, value_parser=less_than_1, short='o')]
    pub inactive_opacity: f32,
}

pub fn parse_args() -> Cli {
    return Cli::parse();
}
