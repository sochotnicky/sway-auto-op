# sway-auto-op

Minimalistic utility for setting opacity for inactive windows in SwayWM. It's a
re-implementation of `inactive-windows-transparency.py` from Sway contrib
directory in Rust.

Two reasons:
- I wanted a small project to learn Rust with
- I wanted minimal resource usage (i.e. no Python interpreter)

# Building

cargo build -r

# Using

Run `sway-auto-op` (optionally with `-o` switch to specify opacity). That's it.
